function sosanh(){
    var soThu1 = document.getElementById("soThu1").value;
    var soThu2 = document.getElementById("soThu2").value;
    var soThu3 = document.getElementById("soThu3").value;
    var tempt=0;
    if(Number(soThu1)> Number(soThu2)){
        tempt =soThu1;
        soThu1=soThu2;
        soThu2=tempt;
    }
    if(Number(soThu1)> Number(soThu3)){
        tempt=soThu1;
        soThu1=soThu3;
        soThu3=tempt;
    }
    if(Number(soThu2)> Number(soThu3)){
        tempt=soThu2;
        soThu2=soThu3;
        soThu3=tempt;
    }
    document.getElementById("result").innerHTML = `
    <p style="color:black; font-size:40px" class="p-2">
    Thứ tự các số là : ${soThu1}, ${soThu2}, ${soThu3} 
    </p >
    `;
}

function xuatCauChao(){
    var user= document.getElementById("user").value;
    if(user=="B"||user=="b"){
        document.getElementById("resultbai2").innerHTML = `
        <p style="color:black; font-size:40px" class="p-2">
        Xin chào Bố
        </p >
        `;
    }

    if(user=="M"||user=="m"){
        document.getElementById("resultbai2").innerHTML = `
        <p style="color:black; font-size:40px" class="p-2">
        Xin chào Mẹ
        </p >
        `;
    }

    if(user=="A"||user=="a"){
        document.getElementById("resultbai2").innerHTML = `
        <p style="color:black; font-size:40px" class="p-2">
        Xin chào Anh Trai
        </p >
        `;
    }

    if(user=="E"||user=="e"){
        document.getElementById("resultbai2").innerHTML = `
        <p style="color:black; font-size:40px" class="p-2">
        Xin chào Em Gái
        </p >
        `;
    }
}

function timSoChanLe(){
    var soNguyen1 = document.getElementById("soNguyen1").value;
    var soNguyen2 = document.getElementById("soNguyen2").value;
    var soNguyen3 = document.getElementById("soNguyen3").value;
    var mangSoChan = [];
    var mangSole=[];
    if(soNguyen1%2===0){
        mangSoChan.push(soNguyen1);
    }else{
        mangSole.push(soNguyen1);
    }

    if(soNguyen2%2===0){
        mangSoChan.push(soNguyen2);
    }else{
        mangSole.push(soNguyen2);
    }

    if(soNguyen3%2===0){
        mangSoChan.push(soNguyen3);
    }else{
        mangSole.push(soNguyen3);
    }

    document.getElementById("resultbai3").innerHTML = `
    <p style="color:black; font-size:40px" class="p-2">
    Số chẳn:${mangSoChan} - Số lẻ:${mangSole}
    </p >
    `;
}

function phanLoaiTamGiac(){
    var chieuDaiCanh1 = document.getElementById("chieuDaiCanh1").value;
    var chieuDaiCanh2 = document.getElementById("chieuDaiCanh2").value;
    var chieuDaiCanh3 = document.getElementById("chieuDaiCanh3").value;
    var max=chieuDaiCanh1;
   if(Number(chieuDaiCanh1) + Number(chieuDaiCanh2)< Number(chieuDaiCanh3)||Number(chieuDaiCanh1)+ Number(chieuDaiCanh3)<Number(chieuDaiCanh2)|| Number(chieuDaiCanh2) + Number(chieuDaiCanh3)<Number(chieuDaiCanh1)){
    document.getElementById("resultbai4").innerHTML = `
    <p style="color:black; font-size:40px" class="p-2">
    Đây không phải là tam giác
    </p >
    `;
   }else if(Number(chieuDaiCanh1)==Number(chieuDaiCanh2)&&Number(chieuDaiCanh2)==Number(chieuDaiCanh3)&&Number(chieuDaiCanh1)==Number(chieuDaiCanh3)){
    document.getElementById("resultbai4").innerHTML = `
    <p style="color:black; font-size:40px" class="p-2">
    Đây là tam giác đều
    </p >
    `;
   }else if(Number(chieuDaiCanh1)==Number(chieuDaiCanh2)||Number(chieuDaiCanh1)==Number(chieuDaiCanh3)||Number(chieuDaiCanh2)==Number(chieuDaiCanh3)){
    document.getElementById("resultbai4").innerHTML = `
    <p style="color:black; font-size:40px" class="p-2">
    Đây là tam giác cân
    </p >
    `;
   }else if(Number(chieuDaiCanh1)* Number(chieuDaiCanh1) + Number(chieuDaiCanh2)*Number(chieuDaiCanh2)==Number(chieuDaiCanh3)*Number(chieuDaiCanh3)||Number(chieuDaiCanh1)*Number(chieuDaiCanh1)+Number(chieuDaiCanh3)*Number(chieuDaiCanh3)==Number(chieuDaiCanh2)*Number(chieuDaiCanh2)||Number(chieuDaiCanh2)*Number(chieuDaiCanh2) + Number(chieuDaiCanh3)*Number(chieuDaiCanh3)==Number(chieuDaiCanh1)*Number(chieuDaiCanh1)){
    document.getElementById("resultbai4").innerHTML = `
    <p style="color:black; font-size:40px" class="p-2">
    Đây là tam giác vuông
    </p >
    `;
   }else{
    document.getElementById("resultbai4").innerHTML = `
    <p style="color:black; font-size:40px" class="p-2">
    Đây là tam giác thường
    </p >
    `;
   }

}